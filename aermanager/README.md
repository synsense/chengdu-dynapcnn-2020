AERMANAGER
==========

Author: Sadique Sheik, SynSense, Zurich

This presentation goes over the process of spike based data handling from event based vision sensors.
In particular we will go over how to load data recorded from such sensors, how to pre-process them, and how to load such data into a Dataset for model training.


Requirements
------------
All python required to run this notebook is listed in requirements.txt. 
You can install them by running the following command.

We recommend using python >= 3.7


```bash
pip install -r requirements.txt
```

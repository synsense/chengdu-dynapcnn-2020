# chengdu-dynapcnn-2020

All interactive workshop material related to the workshop on SynSense DYNAP-CNN devkit.

The participants can use the notebooks to work along with the presenters. 

# Guide

```shell
pip install sinabs sinabs-dynapcnn aermanager
pip install synsense-samna --upgrade && samna-upgrade
sudo cp 60-synsense.rules /etc/udev/rules.d/.
```
